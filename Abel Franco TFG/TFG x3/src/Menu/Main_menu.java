package Menu;

import javax.swing.*;
import java.awt.*;

public class Main_menu extends JFrame {
    private JPanel panel1;
    public JButton crearFacturaButton;
    public JButton articulosButton;
    public JButton DBClientesButton;
    public JButton informesButton;
    public JButton administrarUsuariosButton;

    public Main_menu(){

        initFrame();
        this.setIconImage(getIconImage());
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);

    }
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("icon.png"));

        return retValue;
    }
}
