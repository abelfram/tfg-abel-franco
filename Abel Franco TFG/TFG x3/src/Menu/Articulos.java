package Menu;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Articulos extends JFrame {
    public JTabbedPane tabbedPane1;
    private JPanel panel1;
    public JTable tablaArticulos;
    public JTextField txtNombreArticuloAltaArticulos;
    public JTextField txtMarcaAltaArticulos;
    public JButton altaArticulosButton;
    public JButton modificarArticulosButton;
    public JTextField txtDepartamentoAltaArticulos;
    public JTextField txtPrecioAltaArticulos;
    public JTextField txtNUnidades;
    public JTextField txtNAlmacenAltaArticulos;
    public DatePicker dpFechaLlegadaAltaArticulos;
    public JButton eliminarArticuloButton;
    public JTable tablaModArticulos;
    public JButton buscarPorNombreButton;
    public JTextField txtBuscarNombre;

    public DefaultTableModel dtmArticulos;

    public Articulos() {

        initFrame();
        this.setIconImage(getIconImage());
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(null);

        setTableModels();

    }

    public void setTableModels() {
        this.dtmArticulos = new DefaultTableModel();
        this.tablaArticulos.setModel(dtmArticulos);
        this.tablaModArticulos.setModel(dtmArticulos);
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("icon.png"));


        return retValue;
    }
}
