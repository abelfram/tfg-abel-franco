package Modelo;

import Login.*;
import Menu.DbClientes;
import Menu.Articulos;

public class Limpiar {


    public void limpiarCliente(DbClientes dbClientes) {
        dbClientes.txtDocIdenCliente.setText("");
        dbClientes.txtNombreCliente.setText("");
        dbClientes.txtApellidosCliente.setText("");
        dbClientes.txtCorreoCliente.setText("");
        dbClientes.txtDireccion.setText("");
        dbClientes.comboBoxTratoCliente.setSelectedIndex(-1);
        dbClientes.comboBoxDocIdentCliente.setSelectedIndex(-1);
    }

    public void limpiarEmpleado(Register register) {

        register.txtNombreRegistro.setText("");
        register.txtApellidosRegistro.setText("");
        register.comboBoxPuestoRegistro.setSelectedIndex(-1);
        register.comboBoxEstadoContraactualRegistro.setSelectedIndex(-1);
        register.comboBoxTipoJornadaRegistro.setSelectedIndex(-1);
        register.txtSalarioRegistro.setText("");
        register.passwordFieldRegistro.setText("");
        register.confPasswordFieldRegistro.setText("");
        register.txtDNIRegistro.setText("");
        register.txtCorreoRegistro.setText("");
        register.txtNombreUsuarioRegistro.setText("");
    }

    public void limpiarArticulos(Articulos articulos) {
        articulos.txtNUnidades.setText("");
        articulos.txtDepartamentoAltaArticulos.setText("");
        articulos.dpFechaLlegadaAltaArticulos.setDate(null);
        articulos.txtPrecioAltaArticulos.setText("");
        articulos.txtNAlmacenAltaArticulos.setText("");
        articulos.txtMarcaAltaArticulos.setText("");
        articulos.txtNombreArticuloAltaArticulos.setText("");
    }
}