package Modelo;

public class Clientes {

    private int id_cliente;
    private String dNI;
    private int tipo_doc_identidad;
    private String nombre;
    private String apellidos;
    private String correo_electronico;
    private String direccion;
    private int num_telefonico;
    private int trato;

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getdNI() {
        return dNI;
    }

    public void setdNI(String dNI) {
        this.dNI = dNI;
    }

    public int getTipo_doc_identidad() {
        return tipo_doc_identidad;
    }

    public void setTipo_doc_identidad(int tipo_doc_identidad) {
        this.tipo_doc_identidad = tipo_doc_identidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getNum_telefonico() {
        return num_telefonico;
    }

    public void setNum_telefonico(int num_telefonico) {
        this.num_telefonico = num_telefonico;
    }

    public int getTrato() {
        return trato;
    }

    public void setTrato(int trato) {
        this.trato = trato;
    }



}
