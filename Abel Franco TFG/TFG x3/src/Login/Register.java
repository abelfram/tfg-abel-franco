package Login;

import Modelo.Empleados;
import Modelo.SqlUsuarios;
import Modelo.Hash;
import Enum.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Register extends JFrame {
    private JPanel panel1;
    public JTextField txtNEmpleado;
    public JTextField txtNombreRegistro;
    public JTextField txtApellidosRegistro;
    public JComboBox comboBoxPuestoRegistro;
    public JComboBox comboBoxEstadoContraactualRegistro;
    public JComboBox comboBoxTipoJornadaRegistro;
    public JTextField txtSalarioRegistro;
    public JPasswordField passwordFieldRegistro;
    public JPasswordField confPasswordFieldRegistro;
    public JTextField txtDNIRegistro;
    public JTextField txtCorreoRegistro;
    public JTextField txtNombreUsuarioRegistro;
    public JButton btnRegistrar;
    public JButton btnCancelarRegistro;


    public Register() {
        txtNombreRegistro.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtNombreRegistro.getText().length() > 14) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });


        txtApellidosRegistro.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtApellidosRegistro.getText().length() > 39) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtNombreUsuarioRegistro.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtNombreUsuarioRegistro.getText().length() > 14) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtCorreoRegistro.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtCorreoRegistro.getText().length() > 39) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDNIRegistro.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtDNIRegistro.getText().length() > 8) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        initFrame();
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);

        this.setIconImage(getIconImage());
        setEnumCombobox();
    }

    private void setEnumCombobox(){
        for(Puesto constant : Puesto.values()) { comboBoxPuestoRegistro.addItem(constant.getValor());}
        comboBoxPuestoRegistro.setSelectedIndex(-1);

        for(Tipo_jornada constant : Tipo_jornada.values()){ comboBoxTipoJornadaRegistro.addItem(constant.getValor());}
        comboBoxTipoJornadaRegistro.setSelectedIndex(-1);

        for(Tipo_contrato constant : Tipo_contrato.values()){ comboBoxEstadoContraactualRegistro.addItem(constant.getValor());}
        comboBoxEstadoContraactualRegistro.setSelectedIndex(-1);
    }
    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("icon.png"));


        return retValue;
    }
}
