package Controlador;

import Login.*;
import Menu.*;
import Menu.Articulos;
import Modelo.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener, TableModelListener {

    public Modelo modelo;
    public Login login;
    public Register register;
    public DbClientes dbClientes;
    public SqlUsuarios sqlUsuarios;
    public Empleados empleados;
    public Administrar_usuarios administrar_usuarios;
    public Main_menu main_menu;
    public Crear_Factura crear_factura;
    public Articulos articulos;
    public ArticulosModel articulosModel;
    public Clientes clientes;
    public Limpiar limpiar;
    public Informes informes;

    public ModificarEmpleados modificarEmpleados;
    public Factura factura;

    public boolean refrescar;

    // Le pasamos a la clase controlador los objetos de la clase login y modelo
    public Controlador(Modelo modelo, Login login) {

        inicializarClases();
        this.modelo = modelo;
        this.login = login;
        addActionListeners(this);

        //Metodo para comprobar si hay usuarios con el rango de administrador, en caso de que los haya, saltara el login
        //si no, lo hará el registro
        if (sqlUsuarios.existeAdmin() == true) {
            login.setVisible(true);
        } else {
            register.setVisible(true);
        }

        refrescarTodo();
    }

    //Metodo que nos permite refrescar todas las tablas
    private void refrescarTodo() {
        refrescar = false;
        refrescarCliente();
        refrescarArticulos();
        refrescarEmpleados();
        refrescarFacturas();
    }

    //inicializamos todas las clases
    private void inicializarClases() {
        register = new Register();
        dbClientes = new DbClientes();
        sqlUsuarios = new SqlUsuarios();
        empleados = new Empleados();
        administrar_usuarios = new Administrar_usuarios();
        main_menu = new Main_menu();
        crear_factura = new Crear_Factura();
        articulos = new Articulos();
        clientes = new Clientes();
        limpiar = new Limpiar();
        articulosModel = new ArticulosModel();
        informes = new Informes();

        modificarEmpleados = new ModificarEmpleados();
        factura = new Factura();
    }

    private void addActionListeners(ActionListener listener) {
        //Botones de la pantalla de login
        login.btnAceptar.addActionListener(listener);
        login.cancelarButton.addActionListener(listener);
        //Botones de la pantalla de registro
        register.btnRegistrar.addActionListener(listener);
        register.btnCancelarRegistro.addActionListener(listener);
        //Botones de la pantalla de administracion de usuarios
        administrar_usuarios.btnModificar.addActionListener(listener);
        administrar_usuarios.btnRegistrar.addActionListener(listener);
        //botones de la pantalla de Main menu
        main_menu.crearFacturaButton.addActionListener(listener);
        main_menu.articulosButton.addActionListener(listener);
        main_menu.DBClientesButton.addActionListener(listener);
        main_menu.administrarUsuariosButton.addActionListener(listener);
        //Botones de la pantalla de DbClientes
        dbClientes.crearClienteButton.addActionListener(listener);
        dbClientes.modificarClienteButton.addActionListener(listener);
        dbClientes.cancelarButton.addActionListener(listener);
        dbClientes.eliminarClienteButton.addActionListener(listener);
        dbClientes.buscarPorDNIButton.addActionListener(listener);
        //Botones de la pantalla de Articulos
        articulos.altaArticulosButton.addActionListener(listener);
        articulos.modificarArticulosButton.addActionListener(listener);
        articulos.eliminarArticuloButton.addActionListener(listener);
        articulos.buscarPorNombreButton.addActionListener(listener);
        //Botones de la pantalla Modificar Empleados
        modificarEmpleados.btnEliminar.addActionListener(listener);
        modificarEmpleados.btnModificarEmpleado.addActionListener(listener);
        modificarEmpleados.btnCancelarModificar.addActionListener(listener);
        modificarEmpleados.buscarPorDNIButton.addActionListener(listener);
        //Botones de la pantalla crear factura
        crear_factura.btnConfirmar.addActionListener(listener);
        crear_factura.AgregarFactura.addActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {

            //Cases de la pestaña login
            case "AceptarLogin":

                //Creamos la variable en la que se guardara la contraseña hasheada
                String pass = new String(login.pwdLogin.getPassword());

                //si hay texto, hasheamos la contraseña para hacer la comprobacion en la base de datos
                if (!login.txtUsuarioLogin.getText().equals("") && !pass.equals("")) {
                    //Hasheamos la contraseña
                    String newPass = Hash.sha1(pass);

                    //Cogemos el texto del campo de nombre_usuario, y la contraseña hasheada ya creada
                    empleados.setNombre_usuario(login.txtUsuarioLogin.getText());
                    empleados.setPassword(newPass);

                    //Comprobamos que los datos coincidan con los de la base de datos

                    if (sqlUsuarios.login(empleados)) {
                        String permisos = sqlUsuarios.comprobarUsuario(login.txtUsuarioLogin.getText(), newPass);

                        //Cerramos el login
                        Administrar_usuarios.frmLog = null;
                        login.dispose();

                        //Gestionamos los elementos que serán visibles dependiendo del nivel de permisos

                        if (permisos.equalsIgnoreCase("Tecnico")) {
                            main_menu.setVisible(true);
                            main_menu.administrarUsuariosButton.setEnabled(false);
                        } else if (permisos.equalsIgnoreCase("vendedor")) {
                            main_menu.setVisible(true);
                            main_menu.administrarUsuariosButton.setEnabled(false);
                            articulos.txtNombreArticuloAltaArticulos.setEnabled(false);
                            articulos.txtMarcaAltaArticulos.setEnabled(false);
                            articulos.txtDepartamentoAltaArticulos.setEnabled(false);
                            articulos.txtPrecioAltaArticulos.setEnabled(false);
                            articulos.txtNUnidades.setEnabled(false);
                            articulos.dpFechaLlegadaAltaArticulos.setEnabled(false);
                            articulos.txtNAlmacenAltaArticulos.setEnabled(false);
                            articulos.eliminarArticuloButton.setEnabled(false);
                            articulos.altaArticulosButton.setEnabled(false);
                            articulos.modificarArticulosButton.setEnabled(false);
                        } else if (permisos.equalsIgnoreCase("Administrador")) {
                            main_menu.setVisible(true);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Credenciales incorrectas");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Por favor, rellene todos los campos");
                }
                break;

            //Case que nos permite salir del login
            case "CancelarLogin":
                this.login.dispose();
                break;

            //Cases de la pestaña registro

            //Case que nos permite validar el registro
            case "OKRegistro":

                //guardamos la ruta en una variable, tanto la de contraseña como la de confirmar
                pass = new String(register.passwordFieldRegistro.getPassword());
                String passCon = new String(register.confPasswordFieldRegistro.getPassword());

                //comprobamos los numeros y los campos vacios
                if (comprobarRegistroVacio() || pass.equals("")) {
                    JOptionPane.showMessageDialog(null, "Por favor, rellene todos los campos");
                } else if (!isNumeric(register.txtSalarioRegistro.getText())) {
                    JOptionPane.showMessageDialog(null, "Por favor, ponga numeros en el campo correspondiente");
                } else {

                    if (pass.equals(passCon)) {

                        if (sqlUsuarios.existeNombreUsuario(register.txtNombreUsuarioRegistro.getText()) == 0) {

                            if (sqlUsuarios.existeDniEmpleado(register.txtDNIRegistro.getText()) == 0) {

                                if (sqlUsuarios.esEmail(register.txtCorreoRegistro.getText())) {

                                    //hasheamos la contraseña
                                    String newPass = Hash.sha1(pass);

                                    empleados.setNombre(register.txtNombreRegistro.getText());
                                    empleados.setApellidos(register.txtApellidosRegistro.getText());
                                    empleados.setPuesto_laboral(String.valueOf(register.comboBoxPuestoRegistro.getSelectedItem()));
                                    empleados.setEstado_contrato(String.valueOf(register.comboBoxEstadoContraactualRegistro.getSelectedItem()));
                                    empleados.setTipo_jornada(String.valueOf(register.comboBoxTipoJornadaRegistro.getSelectedItem()));
                                    empleados.setSalario(Double.parseDouble(register.txtSalarioRegistro.getText()));
                                    empleados.setPassword(newPass);
                                    empleados.setDNI(register.txtDNIRegistro.getText());
                                    empleados.setCorreo_electronico(register.txtCorreoRegistro.getText());
                                    empleados.setNombre_usuario(register.txtNombreUsuarioRegistro.getText());

                                    if (sqlUsuarios.insertarEmpleado(empleados)) {
                                        limpiar.limpiarEmpleado(register);
                                        JOptionPane.showMessageDialog(null, "Registro completado exitosamente");
                                        refrescarEmpleados();
                                        main_menu.setVisible(true);

                                    } else {
                                        JOptionPane.showMessageDialog(null, "Error al guardar");
                                    }
                                } else {
                                    JOptionPane.showMessageDialog(null, "El correo electronico no tiene un formato valido");
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Este DNI ya existe");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Este nombre de usuario ya existe");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Por favor, las contraseñas no coinciden, reviselas e intentelo de nuevo");
                    }
                }
                break;

            //Case que retrocede a la ventana anterior al registro
            case "CancelarRegister":
                this.register.dispose();
                break;

            //Cases de la pestaña Main Menu

            //Case que accede a la ventana de Creacion de facturas
            case "NewFactura":
                //Crear_Factura crear_factura = new Crear_Factura();
                crear_factura.setVisible(true);
                break;

            //Case que accede a la ventana de Articulos
            case "ArticulosButton":
                articulos.setVisible(true);
                break;

            //Case que accede a la ventana de DBClientes
            case "DBClientes":
                dbClientes.setVisible(true);
                break;

            //Case que accede a la ventana de Administrar usuarios
            case "AdministrarUsuarios":
                administrar_usuarios.setVisible(true);
                break;

            //Cases de la ventana Administrar usuarios

            //Case que accede a la ventana de Registro de usuarios
            case "RegistrarUsuario":
                register.setVisible(true);
                break;

            //Case que accede a la ventana de Modificacion de usuarios
            case "ModificarUsuarioMenu":
                modificarEmpleados.setVisible(true);
                break;

            //Case para eliminar registros
            case "EliminarRegister":
                sqlUsuarios.borrarEmpleado(Integer.parseInt((String) modificarEmpleados.tablaModificarEmpleados.getValueAt(modificarEmpleados.tablaModificarEmpleados.getSelectedRow(), 0)));
                refrescarEmpleados();
                break;

            //Case que añade un cliente a la base de datos
            case "AnadirCliente":

                //comprobamos los numeros y los campos vacios
                if (comprobarClienteVacio()) {
                    JOptionPane.showMessageDialog(null, "Por favor, rellene todos los campos");
                } else if (!isNumeric(dbClientes.txtNTelefonoCliente.getText())) {
                    JOptionPane.showMessageDialog(null, "Por favor, ponga numeros en el campo correspondiente");
                } else {
                    if (sqlUsuarios.existeNombreCliente(dbClientes.txtNombreCliente.getText()) == 0) {

                        if (sqlUsuarios.existeDniCliente(dbClientes.txtDocIdenCliente.getText()) == 0) {

                            if (sqlUsuarios.esEmail(dbClientes.txtCorreoCliente.getText())) {

                                clientes.setTipo_doc_identidad(dbClientes.comboBoxDocIdentCliente.getSelectedIndex());
                                clientes.setdNI(dbClientes.txtDocIdenCliente.getText());
                                clientes.setNombre(dbClientes.txtNombreCliente.getText());
                                clientes.setApellidos(dbClientes.txtApellidosCliente.getText());
                                clientes.setCorreo_electronico(dbClientes.txtCorreoCliente.getText());
                                clientes.setDireccion(dbClientes.txtDireccion.getText());
                                clientes.setNum_telefonico(Integer.parseInt(dbClientes.txtNTelefonoCliente.getText()));
                                clientes.setTrato(dbClientes.comboBoxTratoCliente.getSelectedIndex());

                                if (sqlUsuarios.insertarCliente(clientes)) {
                                    JOptionPane.showMessageDialog(null, "Registro completado exitosamente");
                                    refrescarCliente();
                                    limpiar.limpiarCliente(dbClientes);
                                    register.setVisible(false);
                                    main_menu.setVisible(true);
                                } else {
                                    JOptionPane.showMessageDialog(null, "Error al guardar");
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "El correo electronico no tiene un formato valido");
                            }

                        } else {
                            JOptionPane.showMessageDialog(null, "Este DNI ya existe");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Este nombre de usuario ya existe");
                    }

                }
                break;
            //Case que accede a la ventana de modificar clientes
            case "ModificarCliente":

                if (comprobarArticulosVacio()) {
                    JOptionPane.showMessageDialog(null, "Por favor, rellene todos los campos");
                } else if (!isNumeric(dbClientes.txtNTelefonoCliente.getText())) {
                    JOptionPane.showMessageDialog(null, "Por favor, ponga numeros en el campo correspondiente");
                } else {
                    sqlUsuarios.actualizarCliente(
                            Integer.parseInt(String.valueOf(dbClientes.comboBoxDocIdentCliente.getSelectedIndex())),
                            String.valueOf(dbClientes.txtDocIdenCliente.getText()),
                            String.valueOf(dbClientes.txtNombreCliente.getText()),
                            String.valueOf(dbClientes.txtApellidosCliente.getText()),
                            String.valueOf(dbClientes.txtCorreoCliente.getText()),
                            String.valueOf(dbClientes.txtDireccion.getText()),
                            Integer.parseInt(dbClientes.txtNTelefonoCliente.getText()),
                            Integer.parseInt(String.valueOf(dbClientes.comboBoxTratoCliente.getSelectedIndex())),
                            Integer.parseInt((String) dbClientes.tablaClientes.getValueAt(dbClientes.tablaClientes.getSelectedRow(), 0)));

                    JOptionPane.showMessageDialog(null, "modificado exitosamente");
                    refrescarCliente();
                    limpiar.limpiarCliente(dbClientes);
                }
                break;

            //case para eliminar clientes
            case "EliminarCliente":
                sqlUsuarios.borrarCliente(Integer.parseInt((String) dbClientes.tablaClientes.getValueAt(dbClientes.tablaClientes.getSelectedRow(), 0)));
                refrescarCliente();
                break;

            //case para dar de alta articulos
            case "AltaArticulos":

                if (comprobarArticulosVacio()) {
                    JOptionPane.showMessageDialog(null, "Por favor, rellene todos los campos");
                } else if (!isNumeric(articulos.txtDepartamentoAltaArticulos.getText()) || !isNumeric(articulos.txtPrecioAltaArticulos.getText())
                        || !isNumeric(articulos.txtNUnidades.getText()) || !isNumeric(articulos.txtNAlmacenAltaArticulos.getText())) {
                    JOptionPane.showMessageDialog(null, "Departamento, precio, almacen y unidades son campos numericos, por favor, introduzca un numero");
                } else {
                    if (sqlUsuarios.existeNombreArticulo(articulos.txtNombreArticuloAltaArticulos.getText()) == 0) {

                        articulosModel.setN_unidades(Integer.parseInt(articulos.txtNUnidades.getText()));
                        articulosModel.setDepartamento(Integer.parseInt(articulos.txtDepartamentoAltaArticulos.getText()));
                        articulosModel.setFecha_llegada(articulos.dpFechaLlegadaAltaArticulos.getDate());
                        articulosModel.setPrecio(Integer.parseInt(articulos.txtPrecioAltaArticulos.getText()));
                        articulosModel.setN_almacen(Integer.parseInt(articulos.txtNAlmacenAltaArticulos.getText()));
                        articulosModel.setMarca(articulos.txtMarcaAltaArticulos.getText());
                        articulosModel.setNombre_articulo(articulos.txtNombreArticuloAltaArticulos.getText());


                        if (sqlUsuarios.insertarArticulos(articulosModel)) {
                            JOptionPane.showMessageDialog(null, "Registro completado exitosamente");
                            refrescarArticulos();
                            limpiar.limpiarArticulos(articulos);
                        } else {
                            JOptionPane.showMessageDialog(null, "Error al guardar");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Este nombre de articulo ya existe");
                    }
                }
                break;

            //Case para buscar a un cliente por el DNI
            case "BuscarDNICliente":
                String buscarDNICliente = dbClientes.txtBuscarDNI.getText();
                try {
                    ResultSet rs = sqlUsuarios.buscarCliente(buscarDNICliente);
                    refrescarCliente();
                    cargarClientes(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            //Case para buscar a un empleado por el DNI
            case "BuscarDNIEmpleado":
                String buscarDNIEmpleados = modificarEmpleados.txtBuscarDNI.getText();
                try {
                    ResultSet rs = sqlUsuarios.buscarEmpleado(buscarDNIEmpleados);
                    refrescarEmpleados();
                    cargarEmpleados(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            //Case para buscar a un articulo por el nombre
            case "BuscarNombreArticulo":
                String buscarNombreArticulo = articulos.txtBuscarNombre.getText();
                try {
                    ResultSet rs = sqlUsuarios.buscarArticulo(buscarNombreArticulo);
                    refrescarArticulos();
                    cargarArticulos(rs);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            //Case para modificar un articulo
            case "ModificarArticulos":

                if (comprobarArticulosVacio()) {
                    JOptionPane.showMessageDialog(null, "Por favor, rellene todos los campos");
                } else if (!isNumeric(articulos.txtNUnidades.getText()) || !isNumeric(articulos.txtDepartamentoAltaArticulos.getText()) ||
                        !isNumeric(articulos.txtPrecioAltaArticulos.getText()) || !isNumeric(articulos.txtNAlmacenAltaArticulos.getText())) {
                    JOptionPane.showMessageDialog(null, "Por favor, rellene los campos numericos");
                } else {
                    sqlUsuarios.actualizarArticulos(
                            Integer.parseInt(articulos.txtNUnidades.getText()),
                            Integer.parseInt(articulos.txtDepartamentoAltaArticulos.getText()),
                            articulos.dpFechaLlegadaAltaArticulos.getDate(),
                            Double.parseDouble(articulos.txtPrecioAltaArticulos.getText()),
                            Integer.parseInt(articulos.txtNAlmacenAltaArticulos.getText()),
                            String.valueOf(articulos.txtMarcaAltaArticulos.getText()),
                            String.valueOf(articulos.txtNombreArticuloAltaArticulos.getText()),
                            Integer.parseInt((String) articulos.tablaModArticulos.getValueAt(articulos.tablaModArticulos.getSelectedRow(), 0)));
                    refrescarArticulos();
                    JOptionPane.showMessageDialog(null, "Registro completado exitosamente");
                }
                break;

            //Case para eliminar un articulo
            case "EliminarArticulo":
                sqlUsuarios.borrarArticulos(Integer.parseInt((String) articulos.tablaModArticulos.getValueAt(articulos.tablaModArticulos.getSelectedRow(), 0)));
                refrescarArticulos();
                break;

            //Case para modificar un empleado
            case "ModificarEmpleado":

                pass = new String(modificarEmpleados.passwordFieldModificar.getPassword());
                String passConMod = new String(modificarEmpleados.confPasswordFieldModificar.getPassword());

                try {
                    if (comprobarModificarVacio()) {
                        JOptionPane.showMessageDialog(null, "Por favor, rellene todos los campos");
                        modificarEmpleados.tablaModificarEmpleados.clearSelection();
                    } else if (!isNumeric(modificarEmpleados.txtSalarioModificar.getText())) {
                        JOptionPane.showMessageDialog(null, "Por favor, salario es el campo numerico");
                    } else {
                        if (pass.equals(passConMod)) {
                            String newPass = Hash.sha1(pass);

                            sqlUsuarios.actualizarEmpleado(
                                    String.valueOf(modificarEmpleados.txtDNIModificar.getText()),
                                    String.valueOf(modificarEmpleados.txtNombreModificar.getText()),
                                    String.valueOf(modificarEmpleados.txtApellidosModificar.getText()),
                                    String.valueOf(modificarEmpleados.txtNombreUsuarioModificar.getText()),
                                    String.valueOf(newPass),
                                    String.valueOf(modificarEmpleados.txtCorreoModificar.getText()),
                                    String.valueOf(modificarEmpleados.comboBoxEstadoContraactualModificar.getSelectedItem()),
                                    String.valueOf(modificarEmpleados.comboBoxPuestoModificar.getSelectedItem()),
                                    String.valueOf(modificarEmpleados.comboBoxTipoJornadaModificar.getSelectedItem()),
                                    Double.parseDouble(modificarEmpleados.txtSalarioModificar.getText()),
                                    Integer.parseInt((String) modificarEmpleados.tablaModificarEmpleados.getValueAt(modificarEmpleados.tablaModificarEmpleados.getSelectedRow(), 0)));

                            refrescarEmpleados();
                        } else {
                            JOptionPane.showMessageDialog(null, "Por favor, las contraseñas no coinciden, reviselas e intentelo de nuevo");
                        }
                    }
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null, "Introduce numeros en los campos que lo requieran");
                    modificarEmpleados.tablaModificarEmpleados.clearSelection();
                }
                break;

            //Case para añadir una factura
            case "AgregarFactura":
                if (comprobarFacturaVacia()) {
                    JOptionPane.showMessageDialog(null, "Por favor, rellene todos los campos");
                } else if (!isNumeric(crear_factura.txtCantidad.getText()) || !isNumeric(crear_factura.precioUnidad.getText())) {
                    JOptionPane.showMessageDialog(null, "Por favor, salario es el campo numerico");
                } else {

                    if (sqlUsuarios.insertarFactura(
                            String.valueOf(crear_factura.comboEmpleado.getSelectedItem()),
                            String.valueOf(crear_factura.comboCliente.getSelectedItem()),
                            String.valueOf(crear_factura.comboArticulo.getSelectedItem()),
                            Integer.parseInt(crear_factura.precioUnidad.getText()),
                            Integer.parseInt(crear_factura.txtCantidad.getText())
                    )) {
                        JOptionPane.showMessageDialog(null, "Factura creada");
                        refrescarFacturas();
                    } else {
                        JOptionPane.showMessageDialog(null, "Error al crear factura");
                    }
                }
                break;

            //Case para generar una factura con su informe
            case "CrearFactura":
                int row = crear_factura.tablaFactura.getSelectedRow();
                String eleccion = String.valueOf(crear_factura.tablaFactura.getValueAt(row, 0));
                informes.informClientCompleto(eleccion);

                break;
        }
    }

    //metodo que nos permite cargar clientes
    private void cargarClientes(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[9];
        dbClientes.dtmClientes.setRowCount(0);

        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);
            fila[7] = resultSet.getObject(8);
            fila[8] = resultSet.getObject(9);

            dbClientes.dtmClientes.addRow(fila);
        }
    }

    //metodo que nos permite cargar empleados
    private void cargarEmpleados(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[11];
        modificarEmpleados.dtmEmpleados.setRowCount(0);

        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);
            fila[7] = resultSet.getObject(8);
            fila[8] = resultSet.getObject(9);
            fila[9] = resultSet.getObject(10);
            fila[10] = resultSet.getObject(11);

            modificarEmpleados.dtmEmpleados.addRow(fila);
        }
    }

    //metodo que nos permite cargar articulos
    private void cargarArticulos(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[8];
        articulos.dtmArticulos.setRowCount(0);

        while (resultSet.next()) {
            fila[0] = resultSet.getObject(1);
            fila[1] = resultSet.getObject(2);
            fila[2] = resultSet.getObject(3);
            fila[3] = resultSet.getObject(4);
            fila[4] = resultSet.getObject(5);
            fila[5] = resultSet.getObject(6);
            fila[6] = resultSet.getObject(7);
            fila[7] = resultSet.getObject(8);


            articulos.dtmArticulos.addRow(fila);
        }
    }

    //metodo que nos permite comprobar si los campos del cliente están vacios
    private boolean comprobarClienteVacio() {
        return dbClientes.txtNombreCliente.getText().isEmpty() ||
                dbClientes.txtApellidosCliente.getText().isEmpty() ||
                dbClientes.txtDocIdenCliente.getText().isEmpty() ||
                dbClientes.txtCorreoCliente.getText().isEmpty() ||
                dbClientes.txtNTelefonoCliente.getText().isEmpty() ||
                dbClientes.comboBoxDocIdentCliente.getSelectedIndex() == -1 ||
                dbClientes.comboBoxTratoCliente.getSelectedIndex() == -1;
    }

    //metodo que nos permite comprobar si los campos del registro están vacios
    private boolean comprobarRegistroVacio() {
        return register.txtNombreRegistro.getText().isEmpty() ||
                register.txtApellidosRegistro.getText().isEmpty() ||
                register.comboBoxPuestoRegistro.getSelectedIndex() == -1 ||
                register.comboBoxEstadoContraactualRegistro.getSelectedIndex() == -1 ||
                register.comboBoxTipoJornadaRegistro.getSelectedIndex() == -1 ||
                register.txtSalarioRegistro.getText().isEmpty() ||
                register.txtDNIRegistro.getText().isEmpty() ||
                register.txtCorreoRegistro.getText().isEmpty() ||
                register.txtNombreUsuarioRegistro.getText().isEmpty();
    }

    //metodo que nos permite comprobar si los campos del modificar están vacios
    private boolean comprobarModificarVacio() {
        return
                modificarEmpleados.txtNombreModificar.getText().isEmpty() ||
                        modificarEmpleados.txtApellidosModificar.getText().isEmpty() ||
                        modificarEmpleados.comboBoxPuestoModificar.getSelectedIndex() == -1 ||
                        modificarEmpleados.comboBoxEstadoContraactualModificar.getSelectedIndex() == -1 ||
                        modificarEmpleados.comboBoxTipoJornadaModificar.getSelectedIndex() == -1 ||
                        modificarEmpleados.txtSalarioModificar.getText().isEmpty() ||
                        modificarEmpleados.txtDNIModificar.getText().isEmpty() ||
                        modificarEmpleados.txtCorreoModificar.getText().isEmpty() ||
                        modificarEmpleados.txtNombreUsuarioModificar.getText().isEmpty();
    }

    //metodo que nos permite comprobar si los campos de articulos están vacios
    private boolean comprobarArticulosVacio() {
        return articulos.txtNombreArticuloAltaArticulos.getText().isEmpty() ||
                articulos.txtMarcaAltaArticulos.getText().isEmpty() ||
                articulos.txtDepartamentoAltaArticulos.getText().isEmpty() ||
                articulos.txtPrecioAltaArticulos.getText().isEmpty() ||
                articulos.txtNUnidades.getText().isEmpty() ||
                articulos.dpFechaLlegadaAltaArticulos.getText().isEmpty() ||
                articulos.txtNAlmacenAltaArticulos.getText().isEmpty();
    }

    //metodo que nos permite comprobar si los campos de factura están vacios
    private boolean comprobarFacturaVacia() {
        return crear_factura.comboEmpleado.getSelectedIndex() == -1 ||
                crear_factura.comboCliente.getSelectedIndex() == -1 ||
                crear_factura.comboArticulo.getSelectedIndex() == -1 ||
                crear_factura.precioUnidad.getText().isEmpty() ||
                crear_factura.txtCantidad.getText().isEmpty();
    }

    //metodo que nos permite refrescar clientes
    public void refrescarCliente() {
        try {
            dbClientes.tablaClientes.setModel(construirTableModelClientes(sqlUsuarios.consultarCliente()));
            crear_factura.comboCliente.removeAllItems();
            for (int i = 0; i < dbClientes.dtmClientes.getRowCount(); i++) {
                crear_factura.comboCliente.addItem(dbClientes.dtmClientes.getValueAt(i, 0) + " - " +
                        dbClientes.dtmClientes.getValueAt(i, 2) + ", " + dbClientes.dtmClientes.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //metodo que nos permite refrescar articulos
    public void refrescarArticulos() {
        try {
            crear_factura.tablaFactura.setModel(construirTableModelArticulos(sqlUsuarios.consultarArticulos()));
            crear_factura.comboArticulo.removeAllItems();
            for (int i = 0; i < articulos.dtmArticulos.getRowCount(); i++) {
                crear_factura.comboArticulo.addItem(articulos.dtmArticulos.getValueAt(i, 0) + " - " +
                        articulos.dtmArticulos.getValueAt(i, 2) + ", " + articulos.dtmArticulos.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //metodo que nos permite refrescar empleados
    public void refrescarEmpleados() {
        try {
            crear_factura.tablaFactura.setModel(construirTableModelEmpleados(sqlUsuarios.consultarEmpleado()));
            crear_factura.comboEmpleado.removeAllItems();
            for (int i = 0; i < modificarEmpleados.dtmEmpleados.getRowCount(); i++) {
                crear_factura.comboEmpleado.addItem(modificarEmpleados.dtmEmpleados.getValueAt(i, 0) + " - " +
                        modificarEmpleados.dtmEmpleados.getValueAt(i, 2) + ", " + modificarEmpleados.dtmEmpleados.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //metodo que nos permite refrescar facturas
    private void refrescarFacturas() {
        try {
            crear_factura.tablaFactura.setModel(construirTableModelFactura(sqlUsuarios.consultarFactura()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //metodo que nos permite construir la tabla de clientes
    public DefaultTableModel construirTableModelClientes(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        dbClientes.dtmClientes.setDataVector(data, columnNames);

        return dbClientes.dtmClientes;

    }

    //metodo que nos permite construir la tabla de empleados
    public DefaultTableModel construirTableModelEmpleados(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        modificarEmpleados.dtmEmpleados.setDataVector(data, columnNames);

        return modificarEmpleados.dtmEmpleados;

    }

    //metodo que nos permite construir la tabla de articulos
    public DefaultTableModel construirTableModelArticulos(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        articulos.dtmArticulos.setDataVector(data, columnNames);

        return articulos.dtmArticulos;

    }

    //metodo que nos permite construir la tabla de factura
    public DefaultTableModel construirTableModelFactura(ResultSet rs) throws SQLException {
        ResultSetMetaData metaData = rs.getMetaData();

        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);
        crear_factura.dtmFactura.setDataVector(data, columnNames);

        return crear_factura.dtmFactura;
    }

    //Campo que nos permite comprobar si el dato introducido es un numero
    public boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            try {
                Float.parseFloat(cadena);
                resultado = true;
            } catch (NumberFormatException excepcion2) {
                resultado = false;
            }
        }
        return resultado;
    }

    //Permite ubicar los datos en las tablas
    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    @Override
    public void tableChanged(TableModelEvent e) {

    }

    @Override
    public void valueChanged(ListSelectionEvent e) {

    }
}