package Enum;

public enum Trato{
    HOMBRE("Hombre"),
    MUJER("Mujer"),
    EMPRESA("Empresa");

    private String valor;

    Trato(String valor){
        this.valor = valor;
    }

    public String getValor(){
        return valor;
    }
}