package Enum;

public enum Tipo_jornada{
    COMPLETA("Completa"),
    PARCIAL("Parcial"),
    PRACTICAS("Practicas"),
    NO_CONTRATADO("No contratado");

    private String valor;

    Tipo_jornada(String valor){
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}