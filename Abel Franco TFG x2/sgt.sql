create database if not exists SGT;
use SGT;

create table if not exists CLIENTES
(
    ID_CLIENTE         INT          not null auto_increment primary key,
    TIPO_DOC_IDENTIDAD INT(5)       not null,
    DNI                VARCHAR(9)   not null,
    NOMBRE             VARCHAR(15)  not null,
    APELLIDOS          VARCHAR(40)  not null,
    CORREO_ELECTRONICO VARCHAR(40),
    DIRECCION          VARCHAR(100) not null,
    NUM_TELEFONICO     INT(11),
    TRATO              INT(2)       not null
);


create table if not exists EMPLEADO
(
    N_EMPLEADO         INT         not null auto_increment primary key,
    DNI                VARCHAR(9)  not null,
    NOMBRE             VARCHAR(15) not null,
    APELLIDOS          VARCHAR(40) not null,
    NOMBRE_USUARIO     VARCHAR(40) not null,
    PWD                VARCHAR(60) not null,
    CORREO_ELECTRONICO VARCHAR(45) not null,
    ESTADO_CONTRATO    VARCHAR(45) not null,
    PUESTO_LABORAL     VARCHAR(45) not null,
    TIPO_JORNADA       VARCHAR(45) not null,
    SALARIO            INT(5)      not null
);

create table if not exists ARTICULOS
(
    ID_ARTICULO     INT         not null auto_increment primary key,
    N_UNIDADES      INT(9)      not null,
    DEPARTAMENTO    INT(4)      not null,
    FECHA_LLEGADA   DATE        not null,
    PRECIO          INT(9)      not null,
    N_ALMACEN       INT(5)      not null,
    MARCA           varchar(40),
    NOMBRE_ARTICULO varchar(40) not null
);

create table if not exists FACTURA
(
    ID_FACTURA         INT not null auto_increment primary key,
    N_EMPLEADO         INT not null,
    ID_CLIENTE         INT not null,
    ID_ARTICULO        INT not null,
    COSTE_TOTAL        INT not null,
    CANTIDAD_ARTICULOS INT not null
);

alter table FACTURA
    add foreign key (N_EMPLEADO) references EMPLEADO (N_EMPLEADO),
    add foreign key (ID_CLIENTE) references CLIENTES (ID_CLIENTE),
    add foreign key (ID_ARTICULO) references ARTICULOS (ID_ARTICULO);