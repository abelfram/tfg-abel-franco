package Enum;

public enum Puesto{
    VENDEDOR("Vendedor"),
    TECNICO("Tecnico"),
    ADMINISTRADOR("Administrador");

    private String valor;

    Puesto(String valor){
        this.valor = valor;
    }

    public String getValor(){
        return valor;
    }
}