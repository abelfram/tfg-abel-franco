package Enum;

public enum Tipo_contrato{

    TEMPORAL("Temporal"),
    INDEFINIDO("Indefinido"),
    PRACTICAS("Practicas"),
    NO_CONTRATADO("No contratado");

    private String valor;

    Tipo_contrato(String valor){
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}