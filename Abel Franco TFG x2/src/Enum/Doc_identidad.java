package Enum;

public enum Doc_identidad{
    //0
    DNI("DNI"),
    NIE("NIE"),
    CIF("CIF");

    private String valor;

    Doc_identidad(String valor){
        this.valor = valor;
    }

    public String getValor(){
        return valor;
    }
}
