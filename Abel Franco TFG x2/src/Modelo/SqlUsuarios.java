package Modelo;

import javax.swing.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SqlUsuarios extends ConexionBBDD {

    //Metodo que nos permite insertar empleados
    public boolean insertarEmpleado(Empleados empleados) {

        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();

        String sql = "INSERT INTO empleado (DNI, NOMBRE, APELLIDOS, NOMBRE_USUARIO, PWD," +
                " CORREO_ELECTRONICO, ESTADO_CONTRATO, PUESTO_LABORAL, TIPO_JORNADA, SALARIO) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setString(1, empleados.getDNI());
            preparedStatement.setString(2, empleados.getNombre());
            preparedStatement.setString(3, empleados.getApellidos());
            preparedStatement.setString(4, empleados.getNombre_usuario());
            preparedStatement.setString(5, empleados.getPassword());
            preparedStatement.setString(6, empleados.getCorreo_electronico());
            preparedStatement.setString(7, empleados.getEstado_contrato());
            preparedStatement.setString(8, empleados.getPuesto_laboral());
            preparedStatement.setString(9, empleados.getTipo_jornada());
            preparedStatement.setDouble(10, empleados.getSalario());
            preparedStatement.execute();
            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    //Metodo que nos permite insertar clientes
    public boolean insertarCliente(Clientes clientes) {

        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();

        String sql = "INSERT INTO CLIENTES (TIPO_DOC_IDENTIDAD, DNI, NOMBRE, APELLIDOS, CORREO_ELECTRONICO, DIRECCION," +
                " NUM_TELEFONICO, TRATO) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, clientes.getTipo_doc_identidad());
            preparedStatement.setString(2, clientes.getdNI());
            preparedStatement.setString(3, clientes.getNombre());
            preparedStatement.setString(4, clientes.getApellidos());
            preparedStatement.setString(5, clientes.getCorreo_electronico());
            preparedStatement.setString(6, clientes.getDireccion());
            preparedStatement.setInt(7, clientes.getNum_telefonico());
            preparedStatement.setInt(8, clientes.getTrato());
            preparedStatement.execute();
            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    //Metodo que nos permite insertar articulos
    public boolean insertarArticulos(ArticulosModel articulos) {

        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();

        String sql = "INSERT INTO articulos (ID_ARTICULO, N_UNIDADES, DEPARTAMENTO, FECHA_LLEGADA, PRECIO, N_ALMACEN," +
                " MARCA, NOMBRE_ARTICULO) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, articulos.getId_articulo());
            preparedStatement.setInt(2, articulos.getN_unidades());
            preparedStatement.setInt(3, articulos.getDepartamento());
            preparedStatement.setDate(4, Date.valueOf(articulos.getFecha_llegada()));
            preparedStatement.setDouble(5, articulos.getPrecio());
            preparedStatement.setInt(6, articulos.getN_almacen());
            preparedStatement.setString(7, articulos.getMarca());
            preparedStatement.setString(8, articulos.getNombre_articulo());
            preparedStatement.execute();
            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    //Metodo que nos permite insertar facturas
    public boolean insertarFactura(String N_empleado, String N_cliente, String Articulo, int Precio_unidad, int Cantidad) {
        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();

        String sql = "INSERT INTO factura (N_EMPLEADO, ID_CLIENTE, ID_ARTICULO, COSTE_TOTAL, CANTIDAD_ARTICULOS) " +
                "VALUES(?, ?, ?, ?, ?)";

        int N_emp = Integer.valueOf(N_empleado.split(" ")[0]);
        int N_cli = Integer.valueOf(N_cliente.split(" ")[0]);
        int Art = Integer.valueOf(Articulo.split(" ")[0]);

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, N_emp);
            preparedStatement.setInt(2, N_cli);
            preparedStatement.setInt(3, Art);
            preparedStatement.setInt(4, Precio_unidad);
            preparedStatement.setInt(5, Cantidad);

            preparedStatement.execute();
            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    //Metodo que nos permite actualizar empleados
    public void actualizarEmpleado(String DNI, String Nombre, String Apellidos, String Nombre_usuario, String Password, String Correo_electronico, String Estado_contrato, String Puesto_laboral, String Tipo_jornada, Double Salario, int Num_empleado) {

        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();

        String sql = "UPDATE empleado SET DNI = ?, NOMBRE = ?, APELLIDOS = ?, NOMBRE_USUARIO = ?, PWD = ?," +
                " CORREO_ELECTRONICO = ?, ESTADO_CONTRATO = ?, PUESTO_LABORAL = ?, TIPO_JORNADA = ?, SALARIO = ? WHERE  N_EMPLEADO = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, DNI);
            preparedStatement.setString(2, Nombre);
            preparedStatement.setString(3, Apellidos);
            preparedStatement.setString(4, Nombre_usuario);
            preparedStatement.setString(5, Password);
            preparedStatement.setString(6, Correo_electronico);
            preparedStatement.setString(7, Estado_contrato);
            preparedStatement.setString(8, Puesto_laboral);
            preparedStatement.setString(9, Tipo_jornada);
            preparedStatement.setDouble(10, Salario);
            preparedStatement.setInt(11, Num_empleado);

            preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //Metodo que nos permite actualizar clientes
    public void actualizarCliente(int TIPO_DOC_IDENTIDAD, String DNI, String NOMBRE, String APELLIDOS, String CORREO_ELECTRONICO, String DIRECCION, int NUM_TELEFONICO, int TRATO, int ID_CLIENTE) {

        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();

        String sql = "UPDATE CLIENTES SET TIPO_DOC_IDENTIDAD = ?, DNI = ?, NOMBRE = ?, APELLIDOS = ?, CORREO_ELECTRONICO = ?, DIRECCION = ?," +
                " NUM_TELEFONICO = ?, TRATO = ? WHERE ID_CLIENTE = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);

            preparedStatement.setInt(1, TIPO_DOC_IDENTIDAD);
            preparedStatement.setString(2, DNI);
            preparedStatement.setString(3, NOMBRE);
            preparedStatement.setString(4, APELLIDOS);
            preparedStatement.setString(5, CORREO_ELECTRONICO);
            preparedStatement.setString(6, DIRECCION);
            preparedStatement.setInt(7, NUM_TELEFONICO);
            preparedStatement.setInt(8, TRATO);
            preparedStatement.setInt(9, ID_CLIENTE);
            preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //Metodo que nos permite actualizar articulos
    public void actualizarArticulos(int N_UNIDADES, int DEPARTAMENTO, LocalDate FECHA_LLEGADA, double PRECIO, int N_ALMACEN, String MARCA, String NOMBRE_ARTICULO, int ID_ARTICULO) {

        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();

        String sql = "UPDATE articulos SET N_UNIDADES = ?, DEPARTAMENTO = ?, FECHA_LLEGADA = ?, PRECIO = ?, N_ALMACEN = ?, MARCA = ?," +
                " NOMBRE_ARTICULO = ? where ID_ARTICULO = ? ";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, N_UNIDADES);
            preparedStatement.setInt(2, DEPARTAMENTO);
            preparedStatement.setDate(3, Date.valueOf(FECHA_LLEGADA));
            preparedStatement.setDouble(4, PRECIO);
            preparedStatement.setInt(5, N_ALMACEN);
            preparedStatement.setString(6, MARCA);
            preparedStatement.setString(7, NOMBRE_ARTICULO);
            preparedStatement.setInt(8, ID_ARTICULO);
            preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    //Metodo que nos permite eliminar empleados
    public boolean borrarEmpleado(int NUM_EMPLEADO) {
        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();
        String sql = "DELETE FROM EMPLEADO WHERE N_EMPLEADO = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, NUM_EMPLEADO);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    //Metodo que nos permite eliminar clientes
    public boolean borrarCliente(int ID_CLIENTES) {
        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();
        String sql = "DELETE FROM CLIENTES WHERE ID_CLIENTE = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, ID_CLIENTES);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    //Metodo que nos permite eliminar articulos
    public boolean borrarArticulos(int ID_ARTICULO) {
        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();
        String sql = "DELETE FROM Articulos WHERE ID_ARTICULO = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, ID_ARTICULO);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    //metodo encargado del logueo
    public boolean login(Empleados empleados) {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        String sql = "SELECT N_EMPLEADO, NOMBRE_USUARIO, PWD, NOMBRE, PUESTO_LABORAL FROM empleado WHERE NOMBRE_USUARIO = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, empleados.getNombre_usuario());
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                if (empleados.getPassword().equals(resultSet.getString(3))) {
                    empleados.setNum_empleado(resultSet.getInt(1));
                    empleados.setNombre(resultSet.getString(4));
                    empleados.setPuesto_laboral(resultSet.getString(5));
                    return true;
                } else {
                    return false;
                }

            }
            return false;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    //metodo encargado de comprobar admins
    public boolean existeAdmin() {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        String sql = "SELECT * FROM empleado WHERE PUESTO_LABORAL = 'Administrador'";

        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    //Metodo que permite comprobar los permisos de usuario
    public String comprobarUsuario(String nombre, String contrasena) {
        String resultado = "SELECT PUESTO_LABORAL FROM empleado WHERE NOMBRE_USUARIO ='" + nombre + "' AND PWD='" + contrasena + "'";
        String permisos = "";
        try {
            Statement st = getConexion().createStatement();
            ResultSet rs = st.executeQuery(resultado);

            if (rs.next()) {
                permisos = rs.getString(1);

            } else {
                JOptionPane.showMessageDialog(null, "El usuario no existe", "Nombre de usuario existente", JOptionPane.ERROR_MESSAGE);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return permisos;
    }

    //Metodo que nos permite comprobar si un nombre ya existe
    public int existeNombreUsuario(String nombre_usuario) {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        String sql = "SELECT count(N_EMPLEADO) FROM empleado WHERE N_EMPLEADO = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nombre_usuario);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }

            return 1;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return 1;
        }
    }

    //Metodo que nos permite comprobar si un nombre ya existe
    public int existeNombreCliente(String nombre) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        String sql = "SELECT count(ID_CLIENTE) FROM CLIENTES WHERE ID_CLIENTE = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nombre);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }

            return 1;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return 1;
        }
    }

    //Metodo que nos permite comprobar si un nombre ya existe
    public int existeNombreArticulo(String NOMBRE_ARTICULO) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        String sql = "SELECT count(ID_ARTICULO) FROM articulos WHERE ID_ARTICULO = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, NOMBRE_ARTICULO);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }

            return 1;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return 1;
        }
    }

    //Metodo que nos permite comprobar si un DNI ya existe
    public int existeDniEmpleado(String DNI) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        String sql = "SELECT count(DNI) FROM empleado WHERE DNI = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, DNI);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }

            return 1;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return 1;
        }
    }

    //Metodo que nos permite comprobar si un DNI ya existe
    public int existeDniCliente(String DNI) {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        String sql = "SELECT count(DNI) FROM clientes WHERE DNI = ?";

        try {
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, DNI);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt(1);
            }

            return 1;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return 1;
        }
    }

    //Metodo que nos permite comprobar si un EMAIL tiene el formato adecuado
    public boolean esEmail(String Correo_electronico) {


        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        Matcher matcher = pattern.matcher(Correo_electronico);
        return matcher.find();
    }

    //Metodo que nos permite consultar clientes
    public ResultSet consultarCliente() throws SQLException {
        String sql = "SELECT concat(ID_CLIENTE) as 'ID', concat(TIPO_DOC_IDENTIDAD) as 'Tipo Documento'," +
                " concat(DNI) as 'Documento de identidad', concat(NOMBRE) as 'Nombre',  concat(APELLIDOS) as 'Apellidos'," +
                " concat(CORREO_ELECTRONICO) as 'Correo Electronico', concat(DIRECCION) as 'Direccion', concat(NUM_TELEFONICO) as 'Numero de Telefono', concat(TRATO) as 'Trato' FROM clientes";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        preparedStatement = connection.prepareStatement(sql);
        resultSet = preparedStatement.executeQuery();
        return resultSet;
    }

    //Metodo que nos permite consultar empleados
    public ResultSet consultarEmpleado() throws SQLException {
        String sql = "SELECT concat(N_EMPLEADO) as 'Nº empleado', concat(DNI) as 'DNI/NIE'," +
                " concat(NOMBRE) as 'Nombre',  concat(APELLIDOS) as 'Apellidos'," +
                " concat(NOMBRE_USUARIO) as 'Nombre usuario', concat(PWD) as 'Contraseña', concat(CORREO_ELECTRONICO) as 'Correo electronico'," +
                " concat(ESTADO_CONTRATO) as 'Estado contraactual', concat(PUESTO_LABORAL) as 'Puesto', concat(TIPO_JORNADA) as 'Jornada', concat(SALARIO) as 'Salario' FROM empleado";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        preparedStatement = connection.prepareStatement(sql);
        resultSet = preparedStatement.executeQuery();
        return resultSet;
    }

    //Metodo que nos permite consultar articulos
    public ResultSet consultarArticulos() throws SQLException {
        String sql = "SELECT concat(ID_ARTICULO) as 'ID', concat(N_UNIDADES) as 'Cantidad'," +
                " concat(DEPARTAMENTO) as 'Departamento', concat(FECHA_LLEGADA) as 'Fecha de llegada',  concat(PRECIO) as 'Precio de venta'," +
                " concat(N_ALMACEN) as 'Almacen', concat(MARCA) as 'Marca', concat(NOMBRE_ARTICULO) as 'Articulo' FROM ARTICULOS";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        preparedStatement = connection.prepareStatement(sql);
        resultSet = preparedStatement.executeQuery();
        return resultSet;
    }

    //Metodo que nos permite consultar factura
    public ResultSet consultarFactura() throws SQLException {
        String sql = "SELECT concat(f.ID_ARTICULO) as 'NºArt', concat(c.NOMBRE) as 'nombre cliente'," +
                "concat(e.NOMBRE) as 'Nombre empleado', concat(f.CANTIDAD_ARTICULOS) as 'Cantidad', concat(f.COSTE_TOTAL) as 'Precio'" +
                "FROM factura as f inner join articulos as a on a.ID_ARTICULO = f.ID_ARTICULO " +
                "inner join empleado as e on e.N_EMPLEADO = f.N_EMPLEADO " +
                "inner join clientes c on c.ID_CLIENTE = f.ID_CLIENTE";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection connection = getConexion();

        preparedStatement = connection.prepareStatement(sql);
        resultSet = preparedStatement.executeQuery();

        return resultSet;
    }

    //Metodo para buscar cliente
    public ResultSet buscarCliente(String DNI) throws SQLException {

        String sql = "SELECT ID_CLIENTE, TIPO_DOC_IDENTIDAD, DNI, NOMBRE, APELLIDOS, CORREO_ELECTRONICO,DIRECCION,  NUM_TELEFONICO, TRATO   FROM clientes WHERE DNI = ?";


        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();

        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, String.valueOf(DNI));
        ResultSet resultSet = preparedStatement.executeQuery();

        return resultSet;
    }

    //Metodo para buscar empleados
    public ResultSet buscarEmpleado(String DNI) throws SQLException {

        String sql = "SELECT N_EMPLEADO, DNI, NOMBRE, APELLIDOS, NOMBRE_USUARIO, PWD, CORREO_ELECTRONICO, ESTADO_CONTRATO, PUESTO_LABORAL, TIPO_JORNADA, SALARIO   FROM empleado WHERE DNI = ?";


        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();

        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, String.valueOf(DNI));
        ResultSet resultSet = preparedStatement.executeQuery();

        return resultSet;
    }

    //Metodo para buscar articulos
    public ResultSet buscarArticulo(String NOMBRE_ARTICULO) throws SQLException {

        String sql = "SELECT ID_ARTICULO, N_UNIDADES, DEPARTAMENTO, FECHA_LLEGADA, PRECIO, N_ALMACEN, MARCA, NOMBRE_ARTICULO   FROM articulos WHERE NOMBRE_ARTICULO = ?";

        PreparedStatement preparedStatement = null;
        Connection connection = getConexion();

        preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, String.valueOf(NOMBRE_ARTICULO));
        ResultSet resultSet = preparedStatement.executeQuery();

        return resultSet;
    }

}