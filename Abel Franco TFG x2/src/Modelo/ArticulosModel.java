package Modelo;

import java.time.LocalDate;
import java.util.Date;

public class ArticulosModel {

    public int id_articulo;
    public int n_unidades;
    public int departamento;
    public LocalDate fecha_llegada;
    public int precio;
    public int n_almacen;
    public String marca ;
    public String nombre_articulo;

    public int getId_articulo() {
        return id_articulo;
    }

    public void setId_articulo(int id_articulo) {
        this.id_articulo = id_articulo;
    }

    public int getN_unidades() {
        return n_unidades;
    }

    public void setN_unidades(int n_unidades) {
        this.n_unidades = n_unidades;
    }

    public int getDepartamento() {
        return departamento;
    }

    public void setDepartamento(int departamento) {
        this.departamento = departamento;
    }

    public LocalDate getFecha_llegada() {
        return fecha_llegada;
    }

    public void setFecha_llegada(LocalDate fecha_llegada) {
        this.fecha_llegada = fecha_llegada;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getN_almacen() {
        return n_almacen;
    }

    public void setN_almacen(int n_almacen) {
        this.n_almacen = n_almacen;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getNombre_articulo() {
        return nombre_articulo;
    }

    public void setNombre_articulo(String nombre_articulo) {
        this.nombre_articulo = nombre_articulo;
    }
}
