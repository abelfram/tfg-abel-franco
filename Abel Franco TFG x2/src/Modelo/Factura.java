package Modelo;

public class Factura {

        private String n_empleado;
        private String n_cliente;
        private String articulo;
        private String precio_unidad;
        private String cantidad;

    public String getN_empleado() {
        return n_empleado;
    }

    public void setN_empleado(String n_empleado) {
        this.n_empleado = n_empleado;
    }

    public String getN_cliente() {
        return n_cliente;
    }

    public void setN_cliente(String n_cliente) {
        this.n_cliente = n_cliente;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public String getPrecio_unidad() {
        return precio_unidad;
    }

    public void setPrecio_unidad(String precio_unidad) {
        this.precio_unidad = precio_unidad;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }
}
