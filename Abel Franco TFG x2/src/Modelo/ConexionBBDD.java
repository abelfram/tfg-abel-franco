package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionBBDD {

    private static String base = "SGT";
    private static String user = "root";
    private static String password = "";
    private static final String url = "jdbc:mysql://localhost:3306/" + base;
    private static Connection con = null;

    //Clase que nos permite establecer la conexión con la base de datos
    public static Connection getConexion() {
        if (con == null) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection(url, user, password);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return con;
    }
}
