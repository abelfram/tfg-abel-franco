package Modelo;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;

import java.util.HashMap;

public class Informes {

    public void informClientCompleto(String eleccion) {

        try {

            //Clase que nos permite generar facturas, informes, llamalo X
            HashMap<String, Object> parametros = new HashMap<>();
            parametros.put("eleccion", eleccion);

            JasperReport reporte = JasperCompileManager.compileReport("sgt.jrxml");
            JasperPrint print = JasperFillManager.fillReport(reporte, parametros, ConexionBBDD.getConexion());
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.show();


            JasperExportManager.exportReportToPdfFile(print, "sgt.pdf");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
