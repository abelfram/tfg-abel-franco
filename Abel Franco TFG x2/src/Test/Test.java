package Test;

import Modelo.ArticulosModel;
import Modelo.Clientes;
import Modelo.Empleados;
import Modelo.SqlUsuarios;
import java.sql.Date;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

public class Test {

    SqlUsuarios sqlUsuarios = new SqlUsuarios();
    Empleados empleados = new Empleados();
    Clientes clientes = new Clientes();
    ArticulosModel articulos = new ArticulosModel();

    @org.junit.jupiter.api.Test
    public void AltaCliente() throws SQLException {
        clientes.setId_cliente(50);
        clientes.setdNI("66666666R");
        clientes.setTipo_doc_identidad(1);
        clientes.setNombre("Adolfo");
        clientes.setApellidos("Hitler Schicklgruber");
        clientes.setCorreo_electronico("GasesAdolfo@ario.GER");
        clientes.setDireccion("Auswitch");
        clientes.setNum_telefonico(123123212);
        clientes.setTrato(1);
        sqlUsuarios.insertarCliente(clientes);
        assertTrue(clientes.getId_cliente()>0);
    }

    @org.junit.jupiter.api.Test
    public void EliminarCliente() throws SQLException {
        sqlUsuarios.borrarCliente(50);
        assertNotNull(sqlUsuarios.buscarCliente("66666666R"));
    }

    @org.junit.jupiter.api.Test
    public void AltaEmpleados() throws SQLException {
        empleados.setNum_empleado(50);
        empleados.setNombre("Francisco");
        empleados.setApellidos("Franco Bahamonte");
        empleados.setPassword("VallaDeCeuta");
        empleados.setTipo_jornada("Completa");
        empleados.setEstado_contrato("Indefinido");
        empleados.setPuesto_laboral("Administrador");
        empleados.setSalario(789789789);
        empleados.setNombre_usuario("PacoPrimero");
        empleados.setDNI("55555555R");
        empleados.setCorreo_electronico("paco@ylosniños.ES");
        sqlUsuarios.insertarEmpleado(empleados);
        assertTrue(empleados.getNum_empleado()>0);
    }

    @org.junit.jupiter.api.Test
    public void EliminarEmpleados() throws SQLException {
        sqlUsuarios.borrarEmpleado(50);
        assertNotNull(sqlUsuarios.buscarEmpleado("55555555R"));
    }

    Date miFecha = new Date(2021, 05, 02);
    @org.junit.jupiter.api.Test
    public void AltaArticulos() throws SQLException {
        articulos.setId_articulo(50);
        articulos.setN_unidades(50);
        articulos.setDepartamento(50);
        articulos.setFecha_llegada(miFecha.toLocalDate());
        articulos.setPrecio(50);
        articulos.setN_almacen(50);
        articulos.setMarca("Avivas");
        articulos.setNombre_articulo("asd");
        sqlUsuarios.insertarArticulos(articulos);
        assertTrue(articulos.getId_articulo()>0);
    }

    @org.junit.jupiter.api.Test
    public void EliminarArticulos() throws SQLException {
        sqlUsuarios.borrarArticulos(50);
        assertNotNull(sqlUsuarios.buscarArticulo("asd"));
    }
}
