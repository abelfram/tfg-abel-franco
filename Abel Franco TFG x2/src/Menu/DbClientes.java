package Menu;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

import Enum.*;

public class DbClientes extends JFrame {
    private JPanel panel1;
    public JTextField txtNombreCliente;
    public JTextArea txtApellidosCliente;
    public JTextField txtNumCliente;
    public JTextField txtDocIdenCliente;
    public JTextField txtCorreoCliente;
    public JTextField txtNTelefonoCliente;
    public JComboBox comboBoxDocIdentCliente;
    public JButton crearClienteButton;
    public JButton cancelarButton;
    public JButton modificarClienteButton;
    public JComboBox comboBoxTratoCliente;
    public JTextField txtDireccion;
    public JTable tablaClientes;
    public JButton eliminarClienteButton;
    public JButton buscarPorDNIButton;
    public JTextField txtBuscarDNI;

    public DefaultTableModel dtmClientes;

    public DbClientes() {

        initFrame();
        this.setIconImage(getIconImage());
    }

    private void initFrame() {

        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(null);


        setEnumCombobox();
        setTableModels();
    }

    public void setTableModels() {
        this.dtmClientes = new DefaultTableModel();
        this.tablaClientes.setModel(dtmClientes);
    }

    private void setEnumCombobox() {
        for (Trato constant : Trato.values()) {
            comboBoxTratoCliente.addItem(constant.getValor());
        }
        comboBoxTratoCliente.setSelectedIndex(-1);

        for (Doc_identidad contant : Doc_identidad.values()) {
            comboBoxDocIdentCliente.addItem(contant.getValor());
        }
        comboBoxDocIdentCliente.setSelectedIndex(-1);
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("icon.png"));


        return retValue;
    }


}
