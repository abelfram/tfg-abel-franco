package Menu;

import Login.Login;
import Login.Register;

import javax.swing.*;
import java.awt.*;

public class Administrar_usuarios extends JFrame {

    public static Login frmLog;
    public static Register frmReg;

    private JPanel panel1;
    public JButton btnRegistrar;
    public JButton btnModificar;

    public Administrar_usuarios(){

        initFrame();
        this.setIconImage(getIconImage());
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);

    }
    private void btnRegistrarActionPerformered(java.awt.event.ActionEvent actionEvent) {

        if (frmReg == null) {
            frmReg = new Register();
            frmReg.setVisible(true);
        }
    }

    //TODO decidir si hacer un modificar o no
    private void btnLogearActionPerformered(java.awt.event.ActionEvent actionEvent) {

        if (frmLog == null) {
            frmLog = new Login();
            frmLog.setVisible(true);
        }
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("icon.png"));


        return retValue;
    }
}
