package Menu;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

import Enum.*;

public class Crear_Factura extends JFrame {
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    public JComboBox comboBoxTrato;
    public JComboBox comboBoxDocIdentidad;
    public JTable tablaFactura;
    public JComboBox comboEmpleado;
    public JComboBox comboCliente;
    public JComboBox comboArticulo;
    public JTextField precioUnidad;
    public JTextField txtCantidad;
    public JButton AgregarFactura;
    public JButton btnConfirmar;

    public DefaultTableModel dtmFactura;


    public Crear_Factura() {
        setTableModels();
        initFrame();
        this.setIconImage(getIconImage());
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(null);
        setTableModels();
        setEnumCombobox();

    }

    public void setTableModels() {
        this.dtmFactura = new DefaultTableModel();
        this.tablaFactura.setModel(dtmFactura);
    }

    private void setEnumCombobox() {
        /*for (Trato constant : Trato.values()) {
            comboBoxTrato.addItem(constant.getValor());
        }
        comboBoxTrato.setSelectedIndex(-1);
*/
       /* for (Doc_identidad contant : Doc_identidad.values()) {
            comboBoxDocIdentidad.addItem(contant.getValor());
        }
        comboBoxDocIdentidad.setSelectedIndex(-1);*/
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("icon.png"));


        return retValue;
    }
}
