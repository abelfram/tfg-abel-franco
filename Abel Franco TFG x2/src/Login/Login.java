package Login;

import Modelo.Empleados;
import Modelo.Hash;
import Modelo.SqlUsuarios;
import Menu.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Login extends JFrame {
    private JPanel panel1;
    public JTextField txtUsuarioLogin;
    public JPasswordField pwdLogin;
    public JButton btnAceptar;
    public JButton cancelarButton;

    public Login() {
        txtUsuarioLogin.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtUsuarioLogin.getText().length() > 14) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        initFrame();
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);

        this.setIconImage(getIconImage());
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("icon.png"));


        return retValue;
    }
}
