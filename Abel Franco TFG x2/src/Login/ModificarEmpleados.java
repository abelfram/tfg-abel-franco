package Login;

import Modelo.Empleados;
import Modelo.SqlUsuarios;
import Modelo.Hash;
import Enum.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class ModificarEmpleados extends JFrame {
    private JPanel panel1;
    public JTextField txtNombreModificar;
    public JTextField txtApellidosModificar;
    public JComboBox comboBoxPuestoModificar;
    public JComboBox comboBoxEstadoContraactualModificar;
    public JComboBox comboBoxTipoJornadaModificar;
    public JTextField txtSalarioModificar;
    public JPasswordField passwordFieldModificar;
    public JTextField txtDNIModificar;
    public JTextField txtCorreoModificar;
    public JTextField txtNombreUsuarioModificar;
    public JButton btnModificarEmpleado;
    public JButton btnCancelarModificar;
    public JPasswordField confPasswordFieldModificar;
    public JTable tablaModificarEmpleados;
    public JButton btnEliminar;
    public JButton buscarPorDNIButton;
    public JTextField txtBuscarDNI;

    public DefaultTableModel dtmEmpleados;

    public ModificarEmpleados() {
        txtNombreModificar.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtNombreModificar.getText().length() > 14) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });


        txtApellidosModificar.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtApellidosModificar.getText().length() > 39) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtNombreUsuarioModificar.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtNombreUsuarioModificar.getText().length() > 14) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtCorreoModificar.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtCorreoModificar.getText().length() > 39) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        txtDNIModificar.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                super.keyTyped(e);
                if (txtDNIModificar.getText().length() > 8) {
                    e.consume();
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
        initFrame();
    }

    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.pack();
        this.setVisible(false);
        this.setSize(new Dimension(this.getWidth() + 200, this.getHeight() + 100));
        this.setLocationRelativeTo(null);

        setEnumCombobox();
        setTableModels();
        this.setIconImage(getIconImage());
    }

    private void setEnumCombobox() {
        for (Puesto constant : Puesto.values()) {
            comboBoxPuestoModificar.addItem(constant.getValor());
        }
        comboBoxPuestoModificar.setSelectedIndex(-1);

        for (Tipo_jornada constant : Tipo_jornada.values()) {
            comboBoxTipoJornadaModificar.addItem(constant.getValor());
        }
        comboBoxTipoJornadaModificar.setSelectedIndex(-1);

        for (Tipo_contrato constant : Tipo_contrato.values()) {
            comboBoxEstadoContraactualModificar.addItem(constant.getValor());
        }
        comboBoxEstadoContraactualModificar.setSelectedIndex(-1);
    }

    public void setTableModels() {
        this.dtmEmpleados = new DefaultTableModel();
        this.tablaModificarEmpleados.setModel(dtmEmpleados);
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("icon.png"));


        return retValue;
    }
}
